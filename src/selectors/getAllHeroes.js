import { heroes } from "../data/heroes"

export const allHeroes = () => {
  return heroes.map(heroe => heroe)
};
