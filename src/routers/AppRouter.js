import React from "react";
import { Routes, Route, BrowserRouter as Router } from "react-router-dom";
import DashboardRoute from "./DashboardRoute";
import { LoginScreen} from '../components/index'


const AppRouter = () => {
  return (
    <Router>
      <Routes>
        <Route path="/login" element={<LoginScreen />} />
        <Route path="/*" element={<DashboardRoute />} /> 
      </Routes>
    </Router>
  );
};

export default AppRouter;
