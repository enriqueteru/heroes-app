import { Navbar } from "../components/NavBar";
import { DcScreen, MarvelScreen, NotFoundSreen ,SearchScreen, HomeScreen, HeroScreen} from '../components/index'
import { Route, Routes } from "react-router-dom";
const DashboardRoute = () => {
  return (
    <div>
      <Navbar />
<Routes >
      <Route  path="marvel" element={<MarvelScreen />} />
      <Route  path="dc" element={<DcScreen />} />
      <Route  path="search" element={<SearchScreen />} />
      <Route  path="hero/:id" element={<HeroScreen />} />
      <Route  path="/" element={<HomeScreen />} />
      <Route  path="*" element={<NotFoundSreen />} />
</Routes>
    </div>
  );
};

export default DashboardRoute;
