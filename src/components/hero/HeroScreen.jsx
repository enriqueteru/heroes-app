import React from 'react';
import { Navigate, useParams } from '../../../node_modules/react-router-dom/index';
import { getHeroById } from '../../selectors/getHeroById';

const HeroScreen = () => {

  const {id}= useParams();
  const findedHero = getHeroById(id);
  const imgHero = `/assets/heroes/${findedHero.id}.jpg`

  if(!findedHero){
  return <Navigate to='/' />
}

  return <div className='flex-2'>
      <div className='flex-2__col'>  
      <h1>{findedHero.superhero}</h1> 
      <p>{findedHero.publisher}</p> 

      
      
      </div>
    
      <div className='flex-2__col'>  <img src={imgHero} alt={findedHero.id} /> </div>
    
  </div>;
};

export default HeroScreen;
