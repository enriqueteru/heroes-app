import React from "react";
import { Link } from "react-router-dom";

const HeroListCard = ({ heroes }) => {
  return (
    <>
      {heroes.map(({ id, superhero, alter_ego }, index) => (
      
        <div
          style={{ background:`linear-gradient(333deg, rgba(0,0,0,3) 10%, rgba(0,0,0,0.6) 100%), url(assets/heroes/${id}.jpg`}}
          className="card"
          key={id}
        >
          
          <h2>{superhero}</h2>
          <small>{alter_ego}</small>
          <Link to={`/hero/${id}`}>
            <button> Ver más...</button>
          </Link>
          
      
        </div>
      ))}
    </>
  );
};

export default HeroListCard;
