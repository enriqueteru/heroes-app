import React from "react";
import { getHeroesByPublisher } from "../../selectors/getHeroesByPubliser";
import HeroListCard from "./HeroListCard";

  const HeroList = ({ publisher }) => {
  const heroes = getHeroesByPublisher(publisher);

  return (
    <div>
      <h2> {publisher} </h2>
      <div className="container">
        <HeroListCard heroes={heroes} />
      </div>
    </div>
  );
};

export default HeroList;
