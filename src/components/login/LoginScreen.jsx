import React from 'react';
import {useNavigate} from 'react-router-dom'
const LoginScreen = () => {
  const navigate = useNavigate();
  const handleLogin = () => { 
    navigate('/dc', { 
      replace: true,
    })
  }
  
  return <div>
  <h1>Login</h1>
  <button onClick={ handleLogin }>Login</button>
</div>;
};

export default LoginScreen;
