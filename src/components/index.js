import DcScreen from "../components/dc/DcScreen";
import LoginScreen from "../components/login/LoginScreen";
import MarvelScreen from "../components/marvel/MarvelScreen";
import SearchScreen from "../components/search/SearchScreen";
import HomeScreen from "./home/HomeScreen";
import NotFoundSreen from "./NotFound/NotFoundSreen";
import HeroScreen from "./hero/HeroScreen";
import HeroList from "./hero/HeroList";


export { 
    DcScreen,
    LoginScreen,
    HomeScreen,
    SearchScreen,
    MarvelScreen,
    NotFoundSreen,
    HeroScreen,
    HeroList,
}