import React from 'react';
import { HeroList } from '../index';

const MarvelScreen = () => {
  return    <div>
  <HeroList publisher="Marvel Comics" />
</div>
};

export default MarvelScreen;
